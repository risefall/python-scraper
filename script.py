import requests
import json
from lxml import html
from selenium import webdriver

iterator = 0
result = []
indexUrl = 'https://www.metricon.com.au/new-home-designs/melbourne?includefloorplans=1&includefacades=0&includepromotiles=1&maxrecords=1000'
apiEndpoint = 'https://www.metricon.com.au/Custom/API/Floorplan/GetFloorplanByFloorplanNodeGUID.ashx'

# Launch the browser
browser = webdriver.Firefox()
browser.get(indexUrl)

# Get the page source, parse as html, match elements using xpath selector
page = browser.page_source
tree = html.fromstring(page)
matched = tree.xpath('//@data-floorplan-info')

# Loop over all matched elements
for item in matched:

  # Convert to JSON object
  json_item = json.loads(item)
  
  for i in json_item:  

    # Get the data we're looking for
    guid = i['floorplan_NodeGUID']
    payload = { 'FloorplanNodeGUID': guid }

    # Send it as a request to the API
    request = requests.get(apiEndpoint, params=payload)

    # Add the response to our results array
    result.append(request.json())
    
  iterator+= 1
  print "Scraped %s of %s" % (iterator, (len(matched)))

# Write to file
with open('data.json', 'w') as file:
  json.dump(result, file)

# Exit the browser so you don't end up with 97 instances of FF open
browser.quit()